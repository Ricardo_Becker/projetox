<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo TITLE_SISTEMA?></title>

    <!-- Bootstrap Core CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/bootstrap/dist/css/bootstrap.min.css rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/metisMenu/dist/metisMenu.min.css rel="stylesheet">

    <!-- DataTables CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/datatables-responsive/css/dataTables.responsive.css rel="stylesheet">

    <!-- Custom CSS -->
    <link href=<?php echo URL_DASHBOARD?>dist/css/sb-admin-2.css rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/morrisjs/morris.css rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">        
        <!-- inicia o header e os menus -->
        <?php
          dashboard_header();
        ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Deletar usuário</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista de usuários cadastrados no sistema
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-danger" role="alert">Atenção, ao deletar este usuário ele não poderá ser recuperado.
                            Caso não queira deletar <a class="alert-link" href="javascript:window.history.back();">clique aqui para retornar.</a></div>
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Sobrenome</th>
                                            <th>Email</th>
                                            <th>Login</th>
                                            <th>Tipo</th>                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 

                                    // monta a grid com os dados vindos do array
                                    //echo $id;
                                    foreach($arrdados as $linha){                                                                    
                                    ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $linha['idusuario'];?></td>
                                            <td><?php echo $linha['nome'];?></td>
                                            <td><?php echo $linha['sobrenome'];?></td>
                                            <td class="center"><?php echo $linha['email'];?></td>
                                            <td class="center"><?php echo $linha['login'];?></td>
                                            <td class="center"><?php echo $linha['tipo'];?></td>
                                            
                                        </tr>
                                    <?php 
                                    }  
                                    ?>
                                        
                                        
                                    </tbody>                                    
                                </table>
                            </div>
                            <!-- /.table-responsive -->                            
                        </div>
                        <!-- /.panel-body -->
                        
                    </div>
                    <!-- /.panel -->
                    <div class="col-lg-4">
                        <a href=<?php echo URL_EXEC ?>/usuario/deleteUsuarioProcess/<?php echo $arrdados[0]['idusuario'];?>><button class="btn btn-outline btn-primary btn-lg btn-block" type="button">Deletar usuário</button></a>
                    </div>    
                </div>
                <!-- /.col-lg-12 -->               
            </div>
            <!-- /.row -->            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/jquery/dist/jquery.min.js></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/bootstrap/dist/js/bootstrap.min.js></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/metisMenu/dist/metisMenu.min.js></script>

    <!-- DataTables JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/datatables/media/js/jquery.dataTables.min.js></script>
    <script src=<?php echo URL_DASHBOARD?>/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js></script>
   

    <!-- Custom Theme JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>dist/js/sb-admin-2.js></script>

</body>

</html>
