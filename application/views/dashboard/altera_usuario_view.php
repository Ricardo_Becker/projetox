<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/bootstrap/dist/css/bootstrap.min.css rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/metisMenu/dist/metisMenu.min.css rel="stylesheet">

    <!-- Custom CSS -->
    <link href=<?php echo URL_DASHBOARD?>dist/css/sb-admin-2.css rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=<?php echo URL_DASHBOARD?>bower_components/font-awesome/css/font-awesome.min.css rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href=<?php echo URL_CSS?>/formValidation.css rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- inicia o header e os menus -->
        <?php
          dashboard_header();
        ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Alterar usuário</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Crie usuários e defina o tipo de permissões dele selecionando o tipo de usuário.
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            <div id="resultado" style="display: none;" class="alert alert-danger" role="alert">Este usuário já existe.</div>
                            <div id="resultado2" style="display: none;" class="alert alert-danger" role="alert">O login deve ter pelo menos 5 caracteres</div>
                            <div id="erro3" style="display: none;" class="alert alert-danger" role="alert">As senhas digitadas não são iguais</div>
                            <div id="erro4" style="display: none;" class="alert alert-danger" role="alert">A senha deve ter 6 ou mais caracteres
                            </div>
                                <form role="form" name="criaUsuario" id="criaUsuario" method="post" action=<?php echo URL_EXEC?>/usuario/alteraUsuarioProcess/<?php echo $idusuario?>>
                                    <div class="col-lg-6">                         
                                        <div class="form-group">
                                            <label>Nome</label>
                                            <input type="text" class="form-control" name="nome" autofocus value="<?php echo $nome;?>" >             
                                        </div>
                                        <div class="form-group">
                                            <label>Sobrenome</label>
                                            <input type="text" class="form-control" name="sobrenome" value=http://www.leroymerlin.com.br/assistente/escolha-o-chuveiro-eletrico-ideal-para-o-seu-banheiro"<?php echo $sobrenome;?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" placeholder="seuemail@provedor.com" name="email" value="<?php echo $email;?>">
                                        </div>
                                        <div class="form-group">
                                            <label>CPF</label>
                                            <input class="form-control" placeholder="000.000.000-00" name="cpf" value="<?php echo $cpf;?>">
                                        </div>                                        
                                        <button type="submit" id="submitButton" class="btn btn-default">Alterar</button>
                                        <button type="reset" class="btn btn-default">Limpar formulário</button>                     
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Login</label>
                                            <input  class="form-control" name="login" id="login" value="<?php echo $login;?>" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Senha</label>
                                            <input id="senha" class="form-control" name="senha" type="password">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmar Senha</label>
                                            <input id="confirmasenha" class="form-control" name="confirmasenha" type="password">
                                        </div>
                                        <div class="form-group">
                                        <label>Tipo de usuário</label>
                                            <select class="form-control" name="tipousuario" value="<?php echo $tipousuario;?>">
                                                <option></option>
                                            <?php 
                                                foreach ($TipoUsuario as $TipoUsuarios)
                                                {                                       
                                            ?>
                                                <option value=<?php echo $TipoUsuarios['idtipo_usuario'] ?>>
                                                    <?php echo $TipoUsuarios['tipo'] ?>
                                                </option>
                                              <?php } ?>  
                                            </select>
                                        </div>
                                    </div>
                                </form>                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   <!-- jQuery -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/jquery/dist/jquery.min.js></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/bootstrap/dist/js/bootstrap.min.js></script>
    
    <!-- Validação de Formularios -->
    <script src=<?php echo URL_JS?>dashboard/formValidation/formValidation.js></script>

    <!-- Validação de Formularios -->
    <script src=<?php echo URL_JS?>dashboard/formValidation/bootstrap.js></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=<?php echo URL_JS?>bootstrap/bootstrap.js></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>bower_components/metisMenu/dist/metisMenu.min.js></script>

    <!-- Custom Theme JavaScript -->
    <script src=<?php echo URL_DASHBOARD?>dist/js/sb-admin-2.js></script>



 <script>


$("#senha").blur(function(){
   var senha         = $("#senha").val();
  // var confirmasenha = $("#confirmasenha").val();

   if(senha.length < 6){

    $('#erro4').show("slow");
    $('#submit').addClass("disabled");
    $('#submit').attr("disabled","disabled");

   }else{
    $('#erro4').hide("slow");
    $('#submit').removeClass("disabled");
    $('#submit').removeAttr("disabled","disabled");

   }

});


$("#confirmasenha").blur(function(){
   var senha         = $("#senha").val();
   var confirmasenha = $("#confirmasenha").val();

   if(senha != confirmasenha){

    $('#erro3').show("slow");
    $('#submit').addClass("disabled");
    $('#submit').attr("disabled","disabled");

   }else{
    $('#erro3').hide("slow");
    $('#submit').removeClass("disabled");
    $('#submit').removeAttr("disabled","disabled");
   }

});

$(document).ready(function() {
    $('#criaUsuario').formValidation({
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        
        
        fields: {
            'nome': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            },
            'sobrenome': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            },
            'email': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    },
                    emailAddress: {
                        message: 'Este endereço não é valido.'
                    }
                }
            },
            'cpf': {
                    validators: {
                        notEmpty: {
                        message: 'Este campo é necessário.'
                    },
                        id: {
                            enabled: true,
                            country: 'BR',
                            message: 'CPF inválido'
                        }
                }
                
            }, 
            'login': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            },           
            'senha': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            }, 
            'confirmasenha': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            },     
            'tipousuario': {
                validators: {
                    notEmpty: {
                        message: 'Este campo é necessário.'
                    }
                }
            }
        }
})
    
});



</script>   




</body>

</html>
