
<?php
//CONFIGURAÇÕES DE BANCO DE DADOS
	define('BD_HOST',     'localhost');
	define('BD_DATABASE', 'projetox');
	define('BD_USER',     'root');
	define('BD_PASSWORD', 'root');

//ENVIO DE E-MAILS
	/*define('EMAIL_FROM',         'bn@conectait.com.br');
	define('EMAIL_FROM_NAME',    'Biblioteca Nacional');
	define('EMAIL_USER',         'bn');
	define('EMAIL_PASSWORD',     '12qwaszx');*/


//CONFIGURAÇÕES DO SISTEMA
	define('TITLE_SISTEMA',       '.:: Projeto X ::.');
	define('NOME_SISTEMA',        'Projeto X');													//título das páginas
	define('PREF_COOKIE_CLIENTE', 'projetox');																		//prefixo dos cookies do sistema
	define('BASE_URL_SISTEMA', 	  'http://localhost/ProjetoX');		 				//url para chamada se controllers
	define('BASE_URL_ARQUIVO',    $_SERVER['DOCUMENT_ROOT'] . '/localhost/ProjetoX/');
	define('URL_EXEC',            BASE_URL_SISTEMA);				 											//url para chamada se controllers
	define('URL_INCLUDE',         BASE_URL_SISTEMA . '/application/include/');									//url do diretorio de includes
	define('URL_CSS',             URL_INCLUDE . 'css/');														//dir arquivos css
	define('URL_JS',              URL_INCLUDE . 'js/');														//dir arquivos js
	define('URL_IMG',             URL_INCLUDE . 'img/');
	define('URL_DASHBOARD',       URL_INCLUDE . 'dashboard/');														//dir de imagens

//SERVICOS
	/*define('MAPS_HOST', 'http://maps.google.com/maps/api/geocode/xml?sensor=false&region=BR');
	define('IDPROGRAMA', 1);                                                        							//código do programa
	define('NOMEPROGRAMA', 'Livro Popular');                                        							//nome do programa
	define('NOME_EDITAL', 'NOME DO EDITAL AQUI'); */                                  							//nome do edital para habilitação

/* End of file config.inc.php */