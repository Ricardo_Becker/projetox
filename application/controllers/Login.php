<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo
*
* <DESCRIÇÃO DA CLASSE AQUI> Exemplo de classe controller.
* 
* @author		<AUTOR>
* @package		application
* @subpackage	controllers.<nome_controller>
* @since		<DATA>
*
*/
class Login extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	/**
	* metodo_exemplo()
	* <DESCRIÇÃO MÉTODO AQUI> Exemplo de método.
	* @param type name
	* @return type name
	*/
	function exec()
	{
		$this->load->model('usuario_model');
		$this->load->helper('estrutura_helper');
		
			
		// Coleta user e senha
		$login = $this->input->post('usuario');
		$senha = md5($this->input->post('senha'));


		$confirmaLogin = $this->usuario_model->validaLogin($login, $senha);

		if($confirmaLogin != FALSE)
		{
			
			$dadosLogin = $this->usuario_model->getDadosUsuariobyLogin($login, $senha);

			$arrSession = Array();
			//$arrSession['idusuario'] = $dadosLogin->idusuario;
			$arrSession['nome'] 	 = $dadosLogin->nome;
			$arrSession['sobrenome'] = $dadosLogin->sobrenome;


			$this->session->set_userdata($arrSession);
			//var_dump($dadosLogin);

			$this->load->view('dashboard/dashboard');
			//die();
		}else
		{
			$this->session->sess_destroy();
			$arrDados['erro'] = 1;
			$this->load->view('home', $arrDados);

		}
	}
}
