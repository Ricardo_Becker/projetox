<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Usuario
*
* Classe referente as ações do módulo de usuário
* 
* @author		Ricardo Becker
* @package		application
* @subpackage	controllers.Usuario
* @since		19/02/2016
*
*/
class Usuario extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	/**
	* index()
	* carrega a index de listagem de usuarios.
	* @param type name
	* @return type name
	*/
	function index()
	{
		$this->load->model("usuario_model");

		//busca os dados de todos os usuários cadastrados no sistema
		$Dados = $this->usuario_model->getDadosUsuario();


		// cria um array e envia para a view os dados em forma de array
		$arrDados = array();
		$arrDados['arrdados'] = $Dados;		

		$this->load->view('/dashboard/usuario',$arrDados);
		
	}

	/**
	* criaUsuario()
	* carrega na tela os dados para a criação de usuario
	* @param type name
	* @return type name
	*/
	function criaUsuario()
	{
		$this->load->model("usuario_model");

		$Dados = $this->usuario_model->getTiposUsuarios();


		// cria um array e envia para a view os dados em forma de array
		$arrDados = array();
		$arrDados['TipoUsuario'] = $Dados;	
		//$arrDados['erro'] = null;
		

				
		$this->load->view('/dashboard/cria_usuario', $arrDados);
		
	}

	/**
	* submitCriausuario()
	* envia para o banco os dados referentes a criação de usuários
	* @param type name
	* @return type name
	*/
	function submitCriausuario()
	{

		// Carrega global model
		$this->load->model("global_model");

		// Carrega o usuario_model
		$this->load->model("usuario_model");


		//print_r($_POST);
		//die;
		

		$ArrDados['nome'] 		= $this->input->post('nome');
		$ArrDados['sobrenome'] 	= $this->input->post('sobrenome');
		$ArrDados['email'] 		= $this->input->post('email');
		$ArrDados['cpf'] 		= $this->input->post('cpf');
		$ArrDados['login'] 		= $this->input->post('login');
		$ArrDados['senha'] 		= md5($this->input->post('senha'));
		$ArrDados['idtipo_usuario'] = $this->input->post('tipousuario');

		$login = $ArrDados['login'];

		$LoginBanco = $this->usuario_model->verificaLoginExiste($login);


		if($LoginBanco == 1)
		{	
			//$this->load->view('/dashboard/cria_usuario');
			redirect('/Usuario/criaUsuario');
		}else{

			// Realiza o insert.
			$this->global_model->insert('usuario', $ArrDados);

			$this->load->view('/dashboard/sucesso_cadastro_usuario');
			
		}
		

	}

	/**
	* verificaLoginExiste()
	* Verica se o login exise via ajax
	* @param 
	* @return 
	*/
	function verificaLoginExiste()
	{
		// Carrega o usuario_model
		$this->load->model("usuario_model");

		$login = $_POST['login'];

		//busca os dados de todos os usuários cadastrados no sistema
		$Dados = $this->usuario_model->verificaLoginExiste($login);

		$a = '<login>' . $Dados .'</login>';

		echo $a;
		
	}

	/**
	* deleteUsuario()
	* Mostra a tela com o usuario selecionado para deleção
	* @param int id_usuario
	* @return 
	*/
	function deleteUsuario($id_usuario)
	{
		// Carrega o usuario_model
		$this->load->model("usuario_model");

		//busca os dados de todos os usuários cadastrados no sistema
		$Dados = $this->usuario_model->getDadosUsuariobyID($id_usuario);
		

		// cria um array e envia para a view os dados em forma de array
		$arrDados = array();
		$arrDados['arrdados'] = $Dados;		

		$this->load->view('/dashboard/delete_usuario_view',$arrDados);
	}

	/**
	* deleteUsuarioProcess()
	* Processa a deleção do usuario pelo id recebido
	* @param int id_usuario
	* @return 
	*/
	function deleteUsuarioProcess($id_usuario)
	{
		// Carrega o usuario_model
		$this->load->model("global_model");

		// Realiza o delete.
		$this->global_model->delete('usuario', 'idusuario ='. $id_usuario);

		redirect('/Usuario/index');
	}

	/**
	* alteraUsuario()
	* Monta a tela para a alteração do usuario conforme o id recebido
	* @param int id_usuario
	* @return 
	*/
	function alteraUsuario($id_usuario)
	{
		// Carrega o usuario_model
		$this->load->model("usuario_model");

		//busca os dados de todos os usuários cadastrados no sistema
		$Dados = $this->usuario_model->getDadosUsuariobyID($id_usuario);

		$tipodados = $this->usuario_model->getTiposUsuarios();

		//print_r($Dados);

		// cria um array e envia para a view os dados em forma de array
		$arrDados = array();
		$arrDados['idusuario']  	= $Dados[0]['idusuario'];	
		$arrDados['nome'] 			= $Dados[0]['nome'];	
		$arrDados['sobrenome'] 		= $Dados[0]['sobrenome'];
		$arrDados['email'] 			= $Dados[0]['email'];
		$arrDados['cpf'] 			= $Dados[0]['cpf'];
		$arrDados['login'] 			= $Dados[0]['login'];
		$arrDados['TipoUsuario'] 	= $tipodados;	
		
		$this->load->view('/dashboard/altera_usuario_view',$arrDados);
	}

	function alteraUsuarioProcess($id_usuario)
	{
		// Carrega o usuario_model
		$this->load->model("global_model");

		//print_r($_POST);
		//die;


		$ArrDados['nome'] 		= $this->input->post('nome');
		$ArrDados['sobrenome'] 	= $this->input->post('sobrenome');
		$ArrDados['email'] 		= $this->input->post('email');
		$ArrDados['cpf'] 		= $this->input->post('cpf');	
		$ArrDados['senha'] 		= md5($this->input->post('senha'));
		$ArrDados['idtipo_usuario'] = $this->input->post('tipousuario');

		// Realiza o delete.
		$this->global_model->update('usuario', $ArrDados, 'idusuario ='. $id_usuario);

		redirect('/Usuario/index');
	}
}