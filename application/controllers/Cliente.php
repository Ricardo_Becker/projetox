<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Cliente
*
* Classe referente as ações do módulo de clientes
* 
* @author		Ricardo Becker
* @package		application
* @subpackage	controllers.Usuario
* @since		19/02/2016
*
*/
class Cliente extends CI_Controller {
	
	/**
	* __construct()
	* @return void
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	/**
	* index()
	* carrega a index de listagem de usuarios.
	* @param type name
	* @return type name
	*/
	function index()
	{
		$this->load->model("cliente_model");

		//busca os dados de todos os usuários cadastrados no sistema
		$Dados = $this->cliente_model->getDadosClientes();


		// cria um array e envia para a view os dados em forma de array
		$arrDados = array();
		$arrDados['arrdados'] = $Dados;		

		$this->load->view('/dashboard/clientes',$arrDados);
		
	}
	
}