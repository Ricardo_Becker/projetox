<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo_Model
*
* <DESCRIÇÃO DA CLASSE AQUI> Exemplo de classe model.
* 
* @author		<AUTOR>
* @package		application
* @subpackage	models.<nome_controller>
* @since		<DATA>
*
*/
class usuario_model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	/**
	* metodo_exemplo()
	* <DESCRIÇÃO MÉTODO AQUI> Exemplo de método.
	* @param type name
	* @return type name
	*/
	public function validaLogin($login, $senha)
	{
		

		$sql = sprintf("SELECT u.LOGIN,
							   u.SENHA 
						FROM usuario u 
						WHERE u.LOGIN = '%s'
						and u.SENHA = '%s'", $login, $senha);

		$dados = $this->db->query($sql);

		if ($dados->num_rows() == 1)
		{
				$dados = 
			$linha = $dados->row();
			return $linha;
		}
		else
		{
			return FALSE;
		}		
	}
	
	public function getDadosUsuariobyLogin($login, $senha){

		$sql = sprintf("SELECT u.* 
						FROM usuario u 
						WHERE u.LOGIN = '%s'
						and u.SENHA = '%s'", $login, $senha);

		$dados = $this->db->query($sql);
		$linha = $dados->row();
		return $linha;

	}

	public function getDadosUsuario(){

		$sql = "SELECT
						u.*,
						tu.tipo
				FROM 	usuario u,
	 					tipo_usuario tu
				WHERE 
						u.idtipo_usuario = tu.idtipo_usuario";

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

	public function getTiposUsuarios(){

		$sql = "SELECT * FROM tipo_usuario";

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

	public function verificaLoginExiste($login){

		$sql = sprintf("SELECT login FROM usuario WHERE login = '%s' ",$login);

		$dados = $this->db->query($sql);
		$linha = $dados->num_rows();
		return $linha;

	}

	public function getDadosUsuariobyID($id_usuario){

		$sql = sprintf("SELECT
						u.*,
						tu.tipo
				FROM 	usuario u,
	 					tipo_usuario tu
				WHERE 
						u.idtipo_usuario = tu.idtipo_usuario
				AND     u.idusuario = '%s'",$id_usuario);

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

}
