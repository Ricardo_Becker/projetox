<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe _Exemplo_Model
*
* <DESCRIÇÃO DA CLASSE AQUI> Exemplo de classe model.
* 
* @author		<AUTOR>
* @package		application
* @subpackage	models.<nome_controller>
* @since		<DATA>
*
*/
class cliente_model extends CI_Model {
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	

	public function getDadosCliente(){

		$sql = "SELECT
						u.*,
						tu.tipo
				FROM 	usuario u,
	 					tipo_usuario tu
				WHERE 
						u.idtipo_usuario = tu.idtipo_usuario";

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

	public function getTiposUsuarios(){

		$sql = "SELECT * FROM tipo_usuario";

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

	public function verificaLoginExiste($login){

		$sql = sprintf("SELECT login FROM usuario WHERE login = '%s' ",$login);

		$dados = $this->db->query($sql);
		$linha = $dados->num_rows();
		return $linha;

	}

	public function getDadosUsuariobyID($id_usuario){

		$sql = sprintf("SELECT
						u.*,
						tu.tipo
				FROM 	usuario u,
	 					tipo_usuario tu
				WHERE 
						u.idtipo_usuario = tu.idtipo_usuario
				AND     u.idusuario = '%s'",$id_usuario);

		$dados = $this->db->query($sql);
		return $dados->result_array();	
	}

}
